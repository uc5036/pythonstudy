from flask import Flask, render_template, request, redirect

app = Flask(__name__)

@app.route('/')
@app.route('/entry')
def entry_page() -> 'html':
    return render_template('entry.html',
                           the_title='Welcome to search4letters on the web')

@app.route('/search4', methods=['POST'])
def do_search() -> 'html':
    phrase=request.form['phrase']
    letters=request.form['letters']
    title='Here are your results:'
    results=str(search4letters(phrase, letters))
    return render_template('results.html',the_phrase=phrase,
                           the_letters=letters,the_title=title,
                           the_results=results,)

def search4vowels(word:str) -> set:
    vowels=set('aeiou')
    return vowels.intersection(set(word))

def search4letters(phrase: str,letters: str='aeiou') -> set:
    return set(letters).intersection(set(phrase))

app.run(debug=True)
